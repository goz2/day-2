## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Today, I primarily focused on learning about task decomposition and building context maps. I also reviewed Git and practiced task decomposition and building context maps with various examples.

- R (Reflective): Unacquainted.

- I (Interpretive): I believe today's class was valuable in enhancing my understanding of task decomposition and the importance of context maps. By breaking down tasks into smaller, manageable components and visualizing the relationships between them, it becomes easier to plan and execute projects effectively. This approach can help improve collaboration and ensure clear communication among team members.

- D (Decisional): I am excited to apply what I learned today in my future projects, especially when working in teams. I will make a conscious effort to apply task decomposition techniques to break down complex tasks into smaller, more manageable units. Additionally, I will leverage context maps to better understand the dependencies and interactions between different components of a project. By implementing these practices, I aim to enhance project organization and efficiency.
