package pos.machine;

import java.util.List;

public class Receipt {
    private List<ReceiptItem> receiptItems;
    private int totalPrice;

    public Receipt(List<ReceiptItem> receiptItemList) {
        this.receiptItems = receiptItemList;
        var total = 0;
        for (ReceiptItem receiptItem : this.receiptItems) {
            total += receiptItem.getSubTotal();
        }
        this.totalPrice = total;
    }

    public Receipt(List<ReceiptItem> receiptItems, int totalPrice) {
        this.receiptItems = receiptItems;
        this.totalPrice = totalPrice;
    }

    public List<ReceiptItem> getReceiptItemList() {
        return receiptItems;
    }

    public void setReceiptItemList(List<ReceiptItem> receiptItemList) {
        this.receiptItems = receiptItemList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
